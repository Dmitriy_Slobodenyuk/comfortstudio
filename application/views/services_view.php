<h1>Услуги</h1>
<h3>Дизайн мебели</h3>
<p>Наши дизайнеры выедут на замеры и создадут дизайн проект будущей мебели. Они учтут все ваши пожелания. И, если
    это необходимо, разработают уникальное изделие, подходящее именно вам.</p>

<h3>Доставка и Установка</h3>
<p>Мы готовы доставить и установить заказанную у нас мебель. Стоимость этой услуги рассчитывается индивидуально. И
    зависит от объема и сложности заказа.
    При самовывозе наши специалисты проконсультируют вас по вопросам сборки и установки.</p>

<p>Более подробную информацию о гарантийном и постгарантийном обслуживании можно получить по телефонам:
    (056) 755-64-40, 096-473-44-59, 093-435-36-37</p>