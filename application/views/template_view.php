<html>
<?Php require_once 'snippets/head.php' ?>
<body>
<?Php require_once 'snippets/navbar.php' ?>
<!-- Page Content -->
<div class="container">
    <div class="row">
        <!-- Blog Sidebar Widgets Column -->
        <div style="margin-top: 25px;" class="col-md-4">

            <!-- Blog Search -->
            <?Php include 'snippets/search.php' ?>

            <!-- side menu -->
            <?Php include 'snippets/side_menu.php' ?>

            <!-- Random Quote -->
            <?Php include 'snippets/quote.php' ?>
        </div>
        <!-- Blog Post Content Column -->
        <div class="col-lg-8">
            <div id="content">
                <div class="box">
                    <?php include 'application/views/' . $content_view; ?>
                </div>
                <br class="clearfix"/>
            </div>
        </div>
    </div>
</div>
<?php include 'snippets/footer.php' ?>
</body>
</html>