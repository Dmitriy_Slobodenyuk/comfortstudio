<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div style="font-size: 25px;color: #e3e3e3;" class="navbar-brand">Comfort Studio</div>
            <!--<a style="font-size: 25px;" class="navbar-brand" href="/">Comfort Studio</a>-->
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav" id="navbar">
                <li>
                    <a id="nvb_main" href="#">ГЛАВНАЯ</a>
                </li>
                <li>
                    <a id="nvb_service" href="#">УСЛУГИ</a>
                </li>
                <li>
                    <a id="nvb_portfolio" href="#">ПОРТФОЛИО</a>
                </li>
                <li>
                    <a id="nvb_contacts" href="#">КОНТАКТЫ</a>
                </li>
            </ul>
        </div>
    </div>
</nav>