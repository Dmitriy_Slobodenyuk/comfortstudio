<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?Php echo SITE_NAME ?></title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?Php echo DIR_ASSETS?>css/bootstrap.min.css"/>
    <link href="<?Php echo DIR_ASSETS?>css/blog-post.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?Php echo DIR_ASSETS?>css/style.css"/>
    <script src="<?Php echo DIR_ASSETS?>js/jquery.js" type="text/javascript"></script>
    <script src="<?Php echo DIR_ASSETS?>js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?Php echo DIR_ASSETS?>js/temp.js" type="text/javascript"></script>
</head>