<?php
define('DIR_ASSETS', "http://phpmvcproject/assets/");
define('ROOT', "http://phpmvcproject/");
define('SITE_NAME', "Comfort Studio");
/*
 * подключаем необходимые файлы
 */
require_once 'core/Model.php';
require_once 'core/View.php';
require_once 'core/Controller.php';
require_once 'core/Route.php';
require_once 'core/DatabaseManager.php';
require_once 'application/core/PortfolioPDO.php';
require_once 'application/models/Portfolio.php';
require_once 'utils/FileNotExistsException.php';
require_once 'models/ModelQuote.php';
require_once 'application/models/find.php';


/*
 * запускаем маршрутизатор
 */
Route::start();
?>