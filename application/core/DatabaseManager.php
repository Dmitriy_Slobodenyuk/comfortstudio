<?php

/*
 * Устанавливает соединение с БД
 */

class DatabaseManager
{
    private $connection = null;

    private $dbConfig = [
        'db' => [
            'host' => '127.0.0.1',
            'user' => 'root',
            'password' => '123',
            'dbName' => 'comfort'
        ]
    ];

    /*
     * set connection with DB and return connection object
     */
    function getConnection()
    {
        try {
            $this->connection = new PDO("mysql:host=" . $this->dbConfig['db']['host'] .
                ";dbname=" . $this->dbConfig['db']['dbName'] . ";charset=UTF8",
                $this->dbConfig['db']['user'], $this->dbConfig['db']['password']);

            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $this->connection;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
}

?>