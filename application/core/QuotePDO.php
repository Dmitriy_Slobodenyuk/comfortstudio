<?php

class QuotePDO
{
    public function __construct()
    {
    }

    /*
     * подключаемся к базе и выбираем все цитаты
     */
    function getAllQuotes()
    {
        try {
            $dbManager = new DatabaseManager();
            $connection = $dbManager->getConnection();
            $sqlQuery = "SELECT quote_text FROM quote";
            $stmt = $connection->prepare($sqlQuery);
            $stmt->execute();
            $row = $stmt->fetchAll();
            $connection = null;

            return $row;
        } catch (PDOException $e) {
            return null;
            //echo 'Error : ' . $e->getMessage();
        }

    }
}
