<?php

class Controller
{
    protected $model;
    protected $view;
    protected $data;

    function __construct()
    {
        $this->view = new View();
        $modelQuote = new ModelQuote();
        $quotes = $modelQuote->get_data();
        $this->data = array();
        $this->data['quotes'] = $quotes;
    }

    function actionIndex()
    {
    }
}
