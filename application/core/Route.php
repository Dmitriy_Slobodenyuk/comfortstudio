<?php

/*
  * Класс-маршрутизатор для определения запрашиваемой страницы.
  * цепляет классы контроллеров и моделей;
  * создает экземпляры контролеров страниц и вызывает действия этих контроллеров.
 */

class Route
{
    const MAIN_CONTROLLER = "Main";
    const INDEX_ACTION = "Index";

    static function start()
    {

        //контроллер и действие по умолчанию
        $controllerName = self::MAIN_CONTROLLER;
        $actionName = self::INDEX_ACTION;

        //разбираем входящий url на массив
        $routes = explode('/', $_SERVER['REQUEST_URI']);

        //получаем имя контроллера
        if (!empty($routes[1])) {
            $controllerName = $routes[1];
        }

        // получаем имя экшена
        if (!empty($routes[2])) {
            $actionName = $routes[2];
        }

        // добавляем префиксы
        $modelName = 'Model' . $controllerName;
        $controllerName = 'Controller' . $controllerName;
        $actionName = 'action' . $actionName;

        // цепляем файл с классом модели
        $modelFile = $modelName . '.php';
        $modelPath = "application/models/" . $modelFile;
        if (file_exists($modelPath)) {
            require_once $modelPath;
        }

        // цепляем файл с классом контроллера
        $controllerFile = $controllerName . '.php';
        $controllerPath = "application/controllers/" . $controllerFile;
        if (file_exists($controllerPath)) {
            require_once $controllerPath;
        } else {
            $controllerPath = "application/controllers/Controller404.php";
            $controllerName = "Controller404";
            require_once $controllerPath;
        }

        // создаем контроллер
        $controller = new $controllerName;
        $action = $actionName;

        if (method_exists($controller, $action)) {
            $controller->$action();
        } else {
            throw new FileNotExistsException("File not found...");
        }

    }
}
