<?php

class View
{
	/*
	 * $content_view - имя файла контента страницы
	 * $template_view - имя файла основного шаблона страницы
	 * $data - массив с доп. параметрами
	 */
	function generate($content_view, $template_view, $data = null)
	{
		require_once 'application/views/'.$template_view;
	}
}
