<?php

class PortfolioPDO
{
    public function __construct()
    {
    }

    /*
     * получаем из базы все записи портфолио, в виде
     * массива обьектов класса Portfolio
     */
    public function getAllItem()
    {
        try {
            $dbManager = new DatabaseManager();
            $connection = $dbManager->getConnection();
            $sqlQuery = "SELECT id, name, description, photo FROM portfolio";
            $stmt = $connection->prepare($sqlQuery);
            $stmt->execute();
            $row = $stmt->fetchAll();
            $connection = null;

            return $this->getListItem($row);
        } catch (PDOException $e) {
            return null;
        }
    }

    public function getByName($name)
    {
        try {
            $dbManager = new DatabaseManager();
            $connection = $dbManager->getConnection();
            $sqlQuery = "SELECT id, name, description, photo FROM portfolio where name LIKE '" . $name . "%' or
            description LIKE '%" . $name . "%'";
            $stmt = $connection->prepare($sqlQuery);
            $stmt->execute();
            $row = $stmt->fetchAll();
            $connection = null;

            return $this->getListItem($row);
        } catch (PDOException $e) {
            return null;
        }
    }

    /*
     * формируем массив обьектов из выборки
     */
    private function getListItem($row)
    {
        $listItem = array();
        if (!empty($row)) {
            foreach ($row as $key => $value) {
                $item = new Portfoliio();
                $item->setId($value['id']);
                $item->setName($value['name']);
                $item->setDescription($value['description']);
                $item->setPhoto($value['photo']);
                $listItem[] = $item;
            }
        }
        return $listItem;
    }
}