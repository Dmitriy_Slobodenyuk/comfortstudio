<?Php
require_once 'application/core/QuotePDO.php';

class ModelQuote extends Model
{

    function __construct()
    {

    }

    public function get_data()
    {
        $quotePDO = new QuotePDO();
        return $quotePDO->getAllQuotes();
    }

}

?>