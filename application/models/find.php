<?Php
/*
 * Файл обрабатывает запрос на поиск записей в портфолио
 */
include_once "../models/Portfolio.php";
include_once "../core/PortfolioPDO.php";
include_once "../core/DatabaseManager.php";

if (is_ajax()) {
    if (isset($_GET["key"]) && !empty($_GET["key"])) {
        /*
         * получаем из запроса введенный юзером текст в текст. поле
         */
        $inputData = $_GET["key"];
        $return = $_GET;
        $findItem = null;
        if (!empty($inputData)) {
            /*
             * отправляемся в базу
             */
            $p = new PortfolioPDO();
            $findItem = $p->getByName($inputData);
        } else {
            $findItem = $p->getAllItem();
        }

        $return['item'] = $findItem;
        echo json_encode($return);
    }
}

//Function to check if the request is an AJAX request
function is_ajax()
{
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}