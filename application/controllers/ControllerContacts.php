<?php

class ControllerContacts extends Controller
{
	function __construct(){
		parent::__construct();
	}
	
	function actionIndex()
	{
		$this->view->generate('contacts_view.php', 'template_view.php', $this->data);
	}
}
