<?php

class ControllerMain extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function actionIndex()
    {
        $this->view->generate('blog.php', 'template_view.php', $this->data);
    }
}