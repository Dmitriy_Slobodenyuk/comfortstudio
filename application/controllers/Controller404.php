<?php

class Controller404 extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function actionIndex()
    {
        $this->data['message'] = "По вашему запросу ничего не найдено !";
        $this->view->generate('404_view.php', 'template_view.php', $this->data);
    }

}
