<?php

class ControllerPortfolio extends Controller
{

	function __construct()
	{
		parent::__construct();
		$this->model = new ModelPortfolio();
	}
	
	function actionIndex()
	{
		$this->data['items'] = $this->model->get_data();
		$this->view->generate('main_view.php', 'template_view.php', $this->data);
	}
}
