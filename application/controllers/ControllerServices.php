<?php

class ControllerServices extends Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function actionIndex()
    {
        $this->view->generate('services_view.php', 'template_view.php', $this->data);
    }
}
