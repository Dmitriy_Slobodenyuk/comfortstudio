$(document).ready(function () {
    getQuote();

    /*
     * обработчик нажатия кнопки поиска
     */
    $("#fnd").click(function () {
        var url = "/application/models/find.php";
        var find = $("input[name='find-input']").val();
        var data = {
            "key": find
        };
        // отправляем запрос
        $.ajax({
            type: "GET",
            dataType: "json",
            url: url,
            data: data,
            /*
             * при успешном запросе получаем в ответ массив
             */
            success: function (data) {
                var dude = data["item"];
                var out = '';
                // формируем выходной контент
                for (var i = 0; i < dude.length; i++) {
                    out += "<p><img src='http://phpmvcproject/assets/images/" + dude[i]['photo'] + "' align='left'>" + dude[i]['description'] + "</p><hr>";
                }
                $(".box").empty().html(
                    "<h1>Каталог Мебели</h1>" + out
                );
            }
        });
    });

    /*
     * Обработчики перехода по ссылке в навбаре
     */
    $("#nvb_main").click(function () {
        var url = "/";
        window.location.href = url;
    });
    $("#nvb_service").click(function () {
        var url = "/Services";
        window.location.href = url;
    });
    $("#nvb_portfolio").click(function () {
        var url = "/Portfolio";
        window.location.href = url;
    });
    $("#nvb_contacts").click(function () {
        var url = "/Contacts";
        window.location.href = url;
    });
});

function random(number) {
    return Math.floor(Math.random() * (number + 1));
}
// получаем случайную цитату
function getQuote() {
    var quotes = $('.quote');
    quotes.hide();
    var qlen = quotes.length;
    $('.quote:eq(' + random(qlen - 1) + ')').show(); //tag:eq(1)
}
